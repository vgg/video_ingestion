video_ingestion application
===========================

Web application for submitting video files to a data processing pipeline.

Author:

 + Ernesto Coto, University of Oxford – <ecoto@robots.ox.ac.uk>

The `video_ingestion` application consist of a `Main Dashboard` that provides access to two main options. A `Submit Video` page from which a user can upload a video file and submit it for processing, and an `Ingestion Records` page which can be used to keep track of the processing of each video.

Basic user administration is provided by Django. When a user is added, the e-mail address of the user must be entered. When a user submits a video for processing, an e-mail will be sent to the user when the processing is finished.

The `Ingestion Records` are presented as a table that can be ordered by video name, user name, submission time, etc. For each submitted video, the user can retrieve a zipped file containing the results of the data processing pipeline, as well a log file with the console output of the processing.

The current version of the application, kept here, corresponds to an application to process videos of chimpanzees, for the purposes of face/body detection and face identification. Nevertheless, the teaser image can be changed, as well as the settings and the data processing pipeline, to adjust it to any other purpose.

The application consists of two main components:

 + The web frontend, included in this repository: It has been developed using **Django version 1.10**, under **Ubuntu 14.04.5 LTS**. The provided version runs in `DEBUG` mode. Should you wish to deploy it to a production environment, please refer to [Django's documentation](https://docs.djangoproject.com/en/1.10/howto/deployment/). The frontend makes use of the free [BootStrap-Agency template](https://startbootstrap.com/themes/agency/).

 + The data processing pipeline: The pipeline is started by the bash script at `pipeline/start_pipeline.sh`. The current version takes the video, extract its frames and then invokes the python script at `pipeline/data_pipeline.sh`. The dependencies of the data pipeline are not included here, see the README file inside the `pipeline` folder. Both scripts can be adjusted to create different data processing pipelines.


