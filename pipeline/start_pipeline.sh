#!/bin/bash
# Parameters:
# $1 -> video_file: Full path to input video
# $2 -> video_log_file_name: Full path to the pipeline log file
# $3 -> video_result_file_name: Full path to the pipeline result file
# $4 -> video_processing_folder_name: Full path to the pipeline temporary processing folder

#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:<additional paths>
BASEDIR=$(dirname "$0")
cd "$BASEDIR"
source ../../bin/activate
#source /webapps/video_ingestion/face_detector/bin/activate
if [ "$#" -eq 4 ]; then
    DATE=`date '+%d-%m-%Y %H:%M:%S'`
    echo "[$DATE]: Executing start_pipeline $1 $2 $3 $4" > "$2"
    # get video name
    VIDEONAME=$(basename "$1")
    # check variable is not empty
    if  [ ! -z "$VIDEONAME" ]; then
        # remove previous temporary folder/file, if present
        rm -rf "$4"
        # make new temporary folders and subfolders for the frames and for the face/body output
        mkdir -p "$4" "$4/${VIDEONAME}" "$4/face" "$4/body"
        # download VIA application
        wget http://www.robots.ox.ac.uk/~vgg/software/via/via.html -P "$4"
        cp "$4/"via.html "$4/face"
        mv "$4/"via.html "$4/body"
        # extract video frames, but only 1 frame per second
        DATE=`date '+%d-%m-%Y %H:%M:%S'`
        echo "[$DATE]: Extracting video frames ..." >> "$2"
        ffmpeg  -i "${1}" -vsync vfr -q:v 1 -start_number 0 -vf scale=iw:ih*\(1/sar\) -loglevel panic "$4/${VIDEONAME}/%06d.jpg" >> "$2" 2>&1
        DATE=`date '+%d-%m-%Y %H:%M:%S'`
        echo "[$DATE]: Finished extracting video frames" >> "$2"
        # make copy on frames folder
        cp -r "$4/${VIDEONAME}" "$4/${VIDEONAME}"_temp
        # call the face detection pipeline. Set ouput_file parameter to -None- to create the ZIP at the end
        echo "[$DATE]: --Starting FACE DETECTION PIPELINE--" >> "$2"
        python data_pipeline.py "${2}" None "$4/${VIDEONAME}" -r -c -i -m /webapps/video_ingestion/face_detector/ssd.pytorch/weights/ssd300_CF_115000.pth
        # move output to face folder
        mv "$4/detections.pkl" "$4/face/"
        mv "$4/"*.csv "$4/face/"
        mv "$4/"*.json "$4/face/"
        mv "$4/${VIDEONAME}" "$4/face/"
        # rename frames folder copy before next pipeline
        mv "$4/${VIDEONAME}"_temp "$4/${VIDEONAME}"
        # call the body detection pipeline. Set ouput_file parameter to -None- to create the ZIP at the end
        echo "[$DATE]: --Starting BODY DETECTION PIPELINE--" >> "$2"
        python data_pipeline.py "${2}" None "$4/${VIDEONAME}" -r -c -m /webapps/video_ingestion/face_detector/ssd.pytorch/weights/ssd300_BFbootstrap2gpu_95000.pth
        # move output to body folder
        mv "$4/detections.pkl" "$4/body/"
        mv "$4/"*.csv "$4/body/"
        mv "$4/"*.json "$4/body/"
        mv "$4/${VIDEONAME}" "$4/body/"
        # create results file
        echo "[$DATE]: --Creating result file--" >> "$2"
        zip -r "$3" "$4" >> "$2" 2>&1
        # clean up
        rm -rf "$4"
        #rm -rf "$1" # keep the video for now
    else
        echo "start_pipeline: video file name missing"
    fi
else 
   echo "start_pipeline: Invalid number of parameters"
fi
