The python files in this repository are included here just for safe-keeping and tracking.

Their dependencies are obviously non included, as well as the pytorch models used.

For 'ssd.pytorch', see https://github.com/amdegroot/ssd.pytorch

For 'svt' see https://gitlab.com/vgg/svt/

The data pipeline runs only with python3 (currently 3.5.2), with the following libraries

numpy (1.15.4)
opencv-python (3.4.4.19)
Pillow  (5.3.0)
pip (18.1)
setuptools (40.6.2)
six (1.11.0)
torch (0.4.1)
torchvision (0.2.1)
wheel (0.32.3)
