#!/bin/sh

#####
## local (test) version
#####
sudo docker run \
-v /webapps/video_ingestion/video_ingestion/results:/webapps/video_ingestion/video_ingestion/results:Z \
-v /webapps/video_ingestion/video_ingestion/logs:/webapps/video_ingestion/video_ingestion/logs:Z \
-v /webapps/video_ingestion/face_detector/ssd.pytorch/weights:/webapps/video_ingestion/face_detector/ssd.pytorch/weights:Z \
-p 127.0.0.1:8000:8000 -it chimp_ingestion /bin/bash

#####
## server version
#####
#sudo docker run --runtime=nvidia --rm \
#-v /scratch/local/hdd/chimp_video_ingestion/database:/webapps/video_ingestion/video_ingestion/database:Z \
#-v /scratch/local/hdd/chimp_video_ingestion/logs:/webapps/video_ingestion/video_ingestion/logs:Z \
#-v /scratch/local/hdd/chimp_video_ingestion/results:/webapps/video_ingestion/video_ingestion/results:Z \
#-v /scratch/local/hdd/chimp_video_ingestion/weights:/webapps/video_ingestion/face_detector/ssd.pytorch/weights:Z \
#-v /scratch/local/hdd/chimp_video_ingestion/django_logs:/webapps/video_ingestion/django_logs/:Z \
#-v /scratch/local/hdd/chimp_video_ingestion/videos:/tmp/videos:Z \
#-v /scratch/local/hdd/chimp_video_ingestion/tmp:/tmp/video_ingestion:Z \
#-p 127.0.0.1:8003:8001 -d chimp_ingestion /webapps/video_ingestion/video_ingestion/scripts/start_all_uwsgi.sh
