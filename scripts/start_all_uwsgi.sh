#!/bin/bash

# restart ngnix
/etc/init.d/nginx restart

# restart rabbitmq
/etc/init.d/rabbitmq-server restart
rabbitmqctl add_user vgg cookies
rabbitmqctl set_permissions -p / vgg ".*" ".*" ".*"
rabbitmqctl set_user_tags vgg administrator
rabbitmqctl delete_user guest

# start celery worker (with/without logfile)
screen -dm -S celery-worker bash -l -c "cd /webapps/video_ingestion/video_ingestion; celery -A video_ingestion worker --concurrency=6 --loglevel INFO -f /webapps/video_ingestion/django_logs/celery_video_ingestion.log"

# start django application
cd /webapps/video_ingestion/video_ingestion
./start_uwsgi_server.sh

# keep docker busy (for docker version)
#tail -f /dev/null
