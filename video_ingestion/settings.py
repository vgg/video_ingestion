"""
Django settings for video_ingestion project.

Generated by 'django-admin startproject' using Django 1.11.16.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
with open( os.path.join(BASE_DIR, '..', 'secret_key_video_ingestion') )  as f:
    SECRET_KEY = f.read().strip()

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS =  []

# Site prefix, if changed:
#   - keep it with the same pattern '/<prefix>'
#   - needs to be replaced too in your web server proxy configuration file (if used).
SITE_PREFIX = "/video_ingestion"

# Full name of the protected resource, to be shown in page
RESOURCE_LONG_NAME = "Chimpanzee Video Ingestion"

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_tables2',
    'siteroot',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'video_ingestion.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'siteroot/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]

WSGI_APPLICATION = 'video_ingestion.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'database', 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/


LANGUAGE_CODE = 'en-uk'

TIME_ZONE = 'GB'

USE_I18N = True

DATETIME_FORMAT = 'd-m-Y H:i:s'

USE_L10N = False

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = SITE_PREFIX + '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, "siteroot/static/")

# Login/logout settings

LOGIN_URL = SITE_PREFIX + '/login/'

LOGIN_REDIRECT_URL = SITE_PREFIX + '/'

LOGOUT_REDIRECT_URL = SITE_PREFIX + '/'

# E-mail settings

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_USE_TLS = False

EMAIL_HOST = 'something.uk'

EMAIL_PORT = 25

EMAIL_HOST_USER = SITE_PREFIX.replace('/','')+ '_donotreply@something.uk'

# Celery settings

USE_CELERY = True

CELERY_BROKER_URL = 'pyamqp://user:passwd@localhost:5672'

CELERY_RESULT_BACKEND = 'rpc://'

CELERY_RESULT_SERIALIZER = 'json'

# Other settings

RESULTS_FILE_EXTENSION = '.zip'

