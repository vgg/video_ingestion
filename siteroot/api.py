# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.conf import settings
from django.http import HttpResponse
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
import os
import time
import threading
import tempfile
from datetime import datetime
from subprocess import Popen, PIPE
from models import Pipeline
import zipfile
import json


def start_pipeline(video_file, pipeline_instance, user_notification):
    """
        Body of the thread that runs the script to start the data ingestion pipeline
        Arguments:
            video_file: full path to local video file
            pipeline_instance: database entry with information about the pipeline
            user_notification: body of the e-mail notification sent to the user when the
                               pipeline is finished
    """
    err = ''
    try:
        video_log_file_name = os.path.join(settings.BASE_DIR, 'logs', pipeline_instance.log_file_name )
        video_result_file_name = os.path.join(settings.BASE_DIR, 'results', pipeline_instance.result_file_name)
        tempfolder_name = pipeline_instance.log_file_name[:-4] # just remove the '.log' part
        video_processing_folder_name = os.path.join(tempfile.gettempdir(), 'video_ingestion', tempfolder_name)
        popen_cmd = [os.path.join(settings.BASE_DIR, 'pipeline', 'start_pipeline.sh'),
                video_file,
                video_log_file_name,
                video_result_file_name,
                video_processing_folder_name]
        popen_obj = Popen(popen_cmd, close_fds=True)
        output, err = popen_obj.communicate()
        pipeline_instance.finished = True
        pipeline_instance.save()
    except Exception as e:
        pipeline_instance.finished = True
        pipeline_instance.result_file_name = "Error:" + str(e)
        pipeline_instance.save()
        err = str(e)
        log_out = open(video_log_file_name, 'w+')
        log_out.write('[%s]: Failed to invoke the script that starts the pipeline\n' %  time.strftime("%d-%m-%Y %H:%M:%S") )
        log_out.write('[%s]: EXCEPTION %s\n' % (time.strftime("%d-%m-%Y %H:%M:%S"), str(e)))
        log_out.close()

    send_mail("[%s] Pipeline finished" % settings.RESOURCE_LONG_NAME, user_notification, settings.EMAIL_HOST_USER, [pipeline_instance.user.email], fail_silently=False)

    return { "pipeline": tempfolder_name, 'error': err }


def create_pipeline_db_entry(username, home_location, video_file_path):
    """
        Creates the database entry for a processing pipeline corresponding to a video uploaded by an user.
        After this, it starts the thread that executes the pipeline.
        Arguments:
            username: name of the user submitting the video
            home_location: URL address of the home site. Can change depending on deployment options.
            video_file_path: local path to the video after the uploading
    """
    # create new pipeline entry in db
    video_submit_time = time.localtime()
    video_pipeline_start_time_str = time.strftime("%d_%m_%Y_%H_%M_%S", video_submit_time)
    user = User.objects.get(username=username)
    video_base_name = os.path.basename(video_file_path)
    log_file_name=video_base_name + "-" + video_pipeline_start_time_str  + '.log'
    result_file_name=video_base_name + "-" + video_pipeline_start_time_str  + settings.RESULTS_FILE_EXTENSION
    pipeline_instance = Pipeline(user=user,
                        video_file_name=video_base_name,
                        start_date_time=datetime.fromtimestamp(time.mktime(video_submit_time)),
                        finished=False,
                        log_file_name=log_file_name,
                        result_file_name=result_file_name
                        )
    pipeline_instance.save()
    # build notification email beforehand
    video_log_file_url = home_location + 'get_file?fname=' + log_file_name
    video_result_file_url = home_location + 'get_file?fname=' + result_file_name
    user_notification = render_to_string('pipeline_finished_email.html', {
            'username': user.username,
            'video_base_name': video_base_name,
            'video_submit_datetime':  time.strftime("%d/%m/%Y %H:%M:%S", video_submit_time),
            'home_location':  home_location,
            'video_log_file_url': video_log_file_url,
            'video_result_file_url': video_result_file_url
        })
    # start thread that starts the processing pipeline
    result = None
    if settings.USE_CELERY:
        result = start_pipeline(video_file_path, pipeline_instance, user_notification)
    else:
        pipeline_start_thread = threading.Thread(target=start_pipeline, args=(video_file_path, pipeline_instance, user_notification))
        pipeline_start_thread.start()

    return json.dumps(result)


if settings.USE_CELERY:
    from celery import shared_task
    @shared_task
    def create_pipeline_db_entry_celery(username, home_location, video_file_path):
        """
            Registers the celery version of create_pipeline_db_entry
            Arguments:
                username: name of the user submitting the video
                home_location: URL address of the home site. Can change depending on deployment options.
                video_file_path: local path to the video after the uploading
        """
        return create_pipeline_db_entry(username, home_location, video_file_path)


def process_zip_file(home_location, user_object, zip_file_path):
    """
        Extracts video files from the specified ZIP and starts/schedules a pipeline execution for each of them
        Arguments:
            home_location: URL address of the home site. Can change depending on deployment options.
            user_object: user information object coming from a Django request
            zip_file_path: local path to the ZIP file after the uploading
    """
    zips_contents_list = []
    video_submit_datetime = time.localtime()
    zip_base_name = os.path.basename(zip_file_path)
    zip_extraction_results = ""
    try:
        the_zip = zipfile.ZipFile(zip_file_path)
        the_zip_contents = the_zip.namelist()
        the_zip_out_folder = os.path.dirname(zip_file_path)
        the_zip.extractall(the_zip_out_folder)
        the_zip.close()
        for video in the_zip_contents:
            full_path = os.path.join(the_zip_out_folder, video)
            if os.path.isfile(full_path):
                zip_extraction_results = zip_extraction_results + video + '\n'
                zips_contents_list.append(full_path)
                if settings.USE_CELERY:
                    create_pipeline_db_entry_celery.delay(user_object.username, home_location, full_path)
                    time.sleep(1)
                else:
                    create_pipeline_db_entry(user_object.username, home_location, full_path)
    except Exception as e:
        zip_extraction_results = zip_extraction_results + '\n' + str(e)
        pass

    # remove the zip file, since the extracted videos will be kept
    try:
        os.remove(zip_file_path)
    except Exception as e:
        zip_extraction_results = zip_extraction_results + '\n' + str(e)
        pass

    user_notification = render_to_string('zip_finished_email.html', {
        'username': user_object.username,
        'zip_base_name': zip_base_name,
        'video_submit_datetime':  time.strftime("%d/%m/%Y %H:%M:%S", video_submit_datetime),
        'home_location':  home_location,
        'zip_extraction_results':  zip_extraction_results
    })

    send_mail("[%s] ZIP file submitted" % settings.RESOURCE_LONG_NAME, user_notification, settings.EMAIL_HOST_USER, [user_object.email], fail_silently=False)


@login_required
def submit_video(request):
    """
        POST request handler used when submitting a video to start a new data ingestion pipeline.
        Uploads the video to the server, create a new pipeline entry in the database, creates
        the body of the user notification e-mail and then start the pipeline in a separate thread.
    """
    if request.method == 'POST':
        if len(request.FILES) == 0:
            message = 'The input file for video ingestion is missing. A pipeline cannot be started.'
            redirect_to = settings.SITE_PREFIX
            return render_to_response("alert_and_redirect.html", context={'REDIRECT_TO': redirect_to, 'MESSAGE': message})
        else:
            try:
                # upload video file
                video_file = request.FILES['input_video']
                tempvideofolder = os.path.join(tempfile.gettempdir(), 'videos')
                if not os.path.exists(tempvideofolder):
                    os.mkdir(tempvideofolder)
                local_path_video_file = os.path.join(tempvideofolder, video_file.name)
                with open(local_path_video_file, 'wb+') as destination:
                    for chunk in video_file.chunks():
                        destination.write(chunk)
                file_extension = os.path.splitext(local_path_video_file)[1]
                home_location = settings.SITE_PREFIX + '/'
                if 'HTTP_X_FORWARDED_HOST' in request.META:
                    home_location = request.META['HTTP_X_FORWARDED_HOST'] + home_location
                else:
                    home_location = 'http://' + get_current_site(request).domain + home_location
                if file_extension == '.zip':
                    # start thread that processes the ZIP file
                    zip_processing_thread = threading.Thread(target=process_zip_file, args=(home_location, request.user, local_path_video_file))
                    zip_processing_thread.start()
                    message = 'Your ZIP file has been uploaded. You will not be able to track the ingestion process of your files \
until the ZIP is decompressed. You will receive an e-mail when all videos have been extracted from the ZIP. \
For each video file inside your ZIP, you will be notified by e-mail when the pipeline finishes.'
                else:
                    if settings.USE_CELERY:
                        create_pipeline_db_entry_celery.delay(request.user.username, home_location, local_path_video_file)
                    else:
                        create_pipeline_db_entry(request.user.username, home_location, local_path_video_file)
                    message = 'Your video has been uploaded and the data ingestion pipeline has started. You will be notified by e-mail when the pipeline finishes.'

                redirect_to = settings.SITE_PREFIX
                return render_to_response("alert_and_redirect.html", context={'REDIRECT_TO': redirect_to, 'MESSAGE': message})
            except Exception as e:
                message = str(e)
                redirect_to = settings.SITE_PREFIX
                return render_to_response("alert_and_redirect.html", context={'REDIRECT_TO': redirect_to, 'MESSAGE': message})
    else:
        message = 'Bad request. POST required.'
        redirect_to = settings.SITE_PREFIX
        return render_to_response("alert_and_redirect.html", context={'REDIRECT_TO': redirect_to, 'MESSAGE': message})


@login_required
def get_file(request):
    """
        GET request handler used when requesting a file.
        It the file is not found, it alerts about it and redirects to the home page.
    """
    fname = request.GET.get('fname', None)
    if not fname:
        message = 'Bad request'
        return render_to_response("alert_and_redirect.html", context={'REDIRECT_TO': settings.SITE_PREFIX, 'MESSAGE': message})

    if fname.endswith('.log'):
        full_fname_path = os.path.join(settings.BASE_DIR, 'logs', fname)
        if os.path.exists(full_fname_path):
            content = open(full_fname_path, 'r').read()
            return HttpResponse(content, content_type='text/plain')
    elif fname.endswith(settings.RESULTS_FILE_EXTENSION):
        full_fname_path = os.path.join(settings.BASE_DIR, 'results', fname)
        if os.path.exists(full_fname_path):
            content = open(full_fname_path, 'r').read()
            response = HttpResponse(content, content_type='application/force-download')
            response['Content-Disposition'] = 'attachment; filename="%s"' % fname
            return response

    # if we reach this point, the file was not found.
    message = 'File %s was not found in the server. Please check the ingestion records.' % fname
    return render_to_response("alert_and_redirect.html", context={'REDIRECT_TO': settings.SITE_PREFIX, 'MESSAGE': message})
