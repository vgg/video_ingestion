# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login as auth_views_login
from django.contrib.auth.views import logout as auth_views_logout
from django.shortcuts import redirect, render
from django.conf import settings
from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport
from models import Pipeline
from tables import PipelineTable


def home(request):
    """ Simple home page showing the dashboard """
    if request.user.is_authenticated():
        context = {
        'STATIC_URL': settings.STATIC_URL,
        'RESOURCE_LONG_NAME': settings.RESOURCE_LONG_NAME,
        }
        return render(request, 'home.html', context)
    else:
        return redirect('login')


@login_required
def submit_view(request):
    """ Simple view with a file selector and a button to submit a video for ingestion """
    home_location = settings.SITE_PREFIX + '/'
    if 'HTTP_X_FORWARDED_HOST' in request.META:
        home_location = 'http://' + request.META['HTTP_X_FORWARDED_HOST'] + home_location
    context = {
    'STATIC_URL': settings.STATIC_URL,
    'HOME_LOCATION': home_location,
    'RESOURCE_LONG_NAME': settings.RESOURCE_LONG_NAME,
    }
    return render(request, 'submit.html', context)


@login_required
def records_view(request):
    """ View with a table showing the records hold for all pipeline executions in the database """
    home_location = settings.SITE_PREFIX + '/'
    if 'HTTP_X_FORWARDED_HOST' in request.META:
        home_location = 'http://' + request.META['HTTP_X_FORWARDED_HOST'] + home_location
    # get the table
    table = PipelineTable(Pipeline.objects.all())
    # by default always sort by the most recent submission date_time
    table.order_by = '-start_date_time'
    # check if user is requestin to export the table
    export_format = request.GET.get('export', None)
    if TableExport.is_valid_format(export_format):
        table.export = True
        exporter = TableExport(export_format, table)
        return exporter.response('table.{}'.format(export_format))
    # check if the user is requesting pagination or not
    page = request.GET.get('page', None)
    if page == 'all':
        RequestConfig(request).configure(table)
    else:
        RequestConfig(request, paginate={'per_page': 15}).configure(table)
    # set the base url for rendering
    table.base_url = home_location
    # render the view with the table
    context = {
    'STATIC_URL': settings.STATIC_URL,
    'HOME_LOCATION': home_location,
    'RESOURCE_LONG_NAME': settings.RESOURCE_LONG_NAME,
    'DISPLAY_WITH_PAGINATION': page != 'all',
    'PIPELINE_TABLE': table
    }
    return render(request, 'records.html', context)


def login(request):
    """ Customized login page to be able to add extra content """
    home_location = settings.SITE_PREFIX + '/'
    if 'HTTP_X_FORWARDED_HOST' in request.META:
        home_location = 'http://' + request.META['HTTP_X_FORWARDED_HOST'] + home_location
    next_location = home_location
    if request.method == 'GET' and 'next' in request.GET:
        next_location = request.GET['next']
    if request.user.is_authenticated():
        return redirect(next_location)
    else:
        context = {
            'STATIC_URL': settings.STATIC_URL,
            'NEXT': next_location,
            'RESOURCE_LONG_NAME': settings.RESOURCE_LONG_NAME
        }
        return auth_views_login(request, template_name='login.html', extra_context=context)


def logout(request):
    """ Customized logout page to be able to specify the next page after login out """
    next_page = None
    try:
        next_page = settings.LOGOUT_REDIRECT_URL
    except:
        next_page = None
        pass
    if not next_page:
        next_page = settings.SITE_PREFIX + '/'
        if 'HTTP_X_FORWARDED_HOST' in request.META:
            next_page = 'http://' + request.META['HTTP_X_FORWARDED_HOST'] + next_page
    return auth_views_logout(request, next_page=next_page)
