# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class Pipeline(models.Model):
    """ Database table holding information about each data ingestion pipeline execution """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    video_file_name = models.CharField(verbose_name='Video File',max_length=100, blank=False, null=False)
    start_date_time = models.DateTimeField(verbose_name='Start date & time', primary_key=True)
    finished = models.BooleanField(verbose_name='Status', default=False)
    log_file_name = models.CharField(verbose_name='Log File', max_length=100, blank=False, null=False)
    result_file_name = models.CharField(verbose_name='Result', max_length=100, blank=False, null=False)
